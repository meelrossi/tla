object Math{
	int hola;
	int calcularFactorial(int n){
		int resultado;
		int i;
		resultado = 1;
		for(i=n;i>1;i=i - 1){
			resultado = resultado * i;
		}
		return resultado;
	}
}


int main(void){
	int numero;
	int factorial;
	numero = -1;
	while(numero <= 0){
		numero = getint("Ingrese un numero entero positivo: ");
	}
	factorial = Math.calcularFactorial(numero);
	printf("El factorial de %d es %d . \n", numero, factorial);
	return 0;
}