object Producto{
	
	char* descricion;
	int precio;
	int stock;

	void init(){
		stock = 24;
		precio = 17;
		descricion = "Pilas AA recargables";
	}

	void info(){
		printf("%s\n", descricion );
		printf("Precio: %d\n", precio);
		printf("Stock: %d\n", stock);
	}

	int comprar(int cantidad){
		if(stock >= cantidad){
			stock = stock - cantidad;
			printf("Se han comprado %d %s\n", cantidad, descricion);
			return cantidad * precio;
		}
		return 0;
	}

}
object Cliente{
	
	int dinero;

	void init(){
	 dinero = 70;
	}

	void comprarProducto(int cantidad){
		printf("Intentando comprar %d Producto/s\n",cantidad );
		dinero = dinero - Producto.comprar(cantidad);
	}

	void balance(){
		printf("Tengo %d pesos.\n", dinero);
	}

}
int main(void){
	Producto.init();
	Cliente.init();
	Cliente.balance();
	Producto.info();
	Cliente.comprarProducto(2);
	Producto.info();
	Cliente.balance();

	return 0;
}