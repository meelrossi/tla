object Tablero{
	int aa;
	int ab;
	int ac;
	int ba;
	int bb;
	int bc;
	int ca;
	int cb;
	int cc;

	void init(){
		int aa = 0;
		int ab = 0;
		int ac = 0;
		int ba = 0;
		int bb = 0;
		int bc = 0;
		int ca = 0;
		int cb = 0;
		int cc = 0;
	}

	int marcar(int columna, int fila, int jugador){
		switch(fila){
			case 1:{
				switch(columna){
					case 1:{
						if(aa == 0){
							aa = jugador;
							return 1;
						} else {
							printf("Lugar ya ocupado.\n");
							return 0;
						}
						break;
					}
					case 2:{
						if(ab == 0){
							ab = jugador;
							return 1;
						} else {
							printf("Lugar ya ocupado.\n");
							return 0;
						}
						break;
					}
					case 3:{
						if(ac == 0){
							ac = jugador;
							return 1;
						} else {
							printf("Lugar ya ocupado.\n");
							return 0;
						}
						break;
					}
					default:{
						break;
					}
				}
			}
			case 2:{
				switch(columna){
					case 1:{
						if(ba == 0){
							ba = jugador;
							return 1;
						} else {
							printf("Lugar ya ocupado.\n");
							return 0;
						}
						break;
					}
					case 2:{
						if(bb == 0){
							bb = jugador;
							return 1;
						} else {
							printf("Lugar ya ocupado.\n");
							return 0;
						}
						break;
					}
					case 3:{
						if(bc == 0){
							bc = jugador;
							return 1;
						} else {
							printf("Lugar ya ocupado.\n");
							return 0;
						}
						break;
					}
					default:{
						break;
					}
				}
			}
			case 3:{
				switch(columna){
					case 1:{
						if(ca == 0){
							ca = jugador;
							return 1;
						} else {
							printf("Lugar ya ocupado.\n");
							return 0;
						}
						break;
					}
					case 2:{
						if(cb == 0){
							cb = jugador;
							return 1;
						} else {
							printf("Lugar ya ocupado.\n");
							return 0;
						}
						break;
					}
					case 3:{
						if(cc == 0){
							cc = jugador;
							return 1;
						} else {
							printf("Lugar ya ocupado.\n");
							return 0;
						}
						break;
					}
					default:{
						break;
					}
				}
			}
			default:{
				break;
			}
		}
		return -1;
	}
	int verificarGanador(){
		if( aa == 1 && ab == 1 && ac == 1){
			return 1;
		}
		if( ba == 1 && bb == 1 && bc == 1){
			return 1;
		}
		if( ca == 1 && cb == 1 && cc == 1){
			return 1;
		}
		if( aa == 1 && ba == 1 && ca == 1){
			return 1;
		}
		if( ab == 1 && bb == 1 && cb == 1){
			return 1;
		}
		if( ac == 1 && bc == 1 && cc == 1){
			return 1;
		}
		if( aa == 1 && bb == 1 && cc == 1){
			return 1;
		}
		if( ac == 1 && bb == 1 && ca == 1){
			return 1;
		}
		if( aa == 2 && ab == 2 && ac == 2){
			return 2;
		}
		if( ba == 2 && bb == 2 && bc == 2){
			return 2;
		}
		if( ca == 2 && cb == 2 && cc == 2){
			return 2;
		}
		if( aa == 2 && ba == 2 && ca == 2){
			return 2;
		}
		if( ab == 2 && bb == 2 && cb == 2){
			return 2;
		}
		if( ac == 2 && bc == 2 && cc == 2){
			return 2;
		}
		if( aa == 2 && bb == 2 && cc == 2){
			return 2;
		}
		if( ac == 2 && bb == 2 && ca == 2){
			return 2;
		}
		return 0;
	}

}

object Jugador1{
	void hacerMovimiento(){
		int fila;
		int columna;
		int aux;
		aux=0;
		while( aux == 0){
			fila = 0;
			columna = 0;
			printf("Movimiento del Jugador1:\n");
			while(columna < 1 || columna > 3){
				columna = getint("Eliga columna:");
			}
			while(fila < 1 || fila > 3){
				fila = getint("Eliga fila:");
			}	
			aux=Tablero.marcar(columna, fila, 1);	
		}
	}
}

object Jugador2{
	void hacerMovimiento(){
		int fila;
		int columna;
		int aux;
		aux = 0;
		while( aux == 0){
			fila = 0;
			columna = 0;
			printf("Movimiento del Jugador2:\n");
			while(columna < 1 || columna > 3){
				columna = getint("Eliga columna:");
			}
			while(fila < 1 || fila > 3){
				fila = getint("Eliga fila:");
			}	
			aux = Tablero.marcar(columna, fila, 2);	
		}
	}
}

object Juego{
	
	int jugadas;
	int aux;

	int jugar(){
		aux = 0;
		jugadas = 0;
		while(1){
			Jugador1.hacerMovimiento();
			aux = Tablero.verificarGanador();
			if (aux != 0)
			{
				return aux;
			}
			jugadas = jugadas + 1;
			if(jugadas == 9){
				return 3;
			}
			Jugador2.hacerMovimiento();
			aux = Tablero.verificarGanador();
			if (aux != 0)
			{
				return aux;
			}
			jugadas = jugadas + 1;
			if(jugadas == 9){
				return 3;
			}
		}
		return -1;
	}

}

int main (void){
	int resultado;
	resultado = Juego.jugar();
	switch (resultado) {
		case 1:{
			printf("Ha ganado el Jugador1\n");
			break;
		}
		case 2:{
			printf("Ha ganado el Jugador2\n");
			break;
		}
		case 3:{
			printf("Han empatado!\n");
			break;
		}
		default :{
			break;
		}
	}
	return 0;
}